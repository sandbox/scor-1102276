#!/bin/sh

TARGET_DIR=$1
TEMPFILE=/tmp/tempfile

if [ ! -d "$TARGET_DIR" ]; then
  echo "Usage: change.sh directory"
  exit 1
fi

cp $TARGET_DIR/bootstrap.inc $TEMPFILE
sed -e "s?include_once DRUPAL_CORE . '/' . \$filename?include_once DRUPAL_ROOT . '/' . \$filename?" $TEMPFILE > $TARGET_DIR/bootstrap.inc

cp $TARGET_DIR/bootstrap.inc $TEMPFILE
sed -e "s?include_once DRUPAL_CORE . '/' . conf_path() . '/settings.php'?include_once DRUPAL_ROOT . '/' . conf_path() . '/settings.php'?" $TEMPFILE > $TARGET_DIR/bootstrap.inc

cp $TARGET_DIR/install.core.inc $TEMPFILE
sed -e "s?include_once DRUPAL_CORE . '/' . \$profile->uri?include_once \$profile->uri?" $TEMPFILE > $TARGET_DIR/install.core.inc

cp $TARGET_DIR/install.inc $TEMPFILE
sed -e "s?require_once DRUPAL_CORE . '/' . \$system_path . '/system.install'?require_once \$system_path . '/system.install'?" $TEMPFILE > $TARGET_DIR/install.inc

#!/usr/bin/env php
<?php

$argc = $_SERVER['argc'];

if ($argc == 2) {
  $target_dir = $_SERVER['argv'][1];
}
else {
  $myname = $_SERVER['argv'][0];
  print "Usage: $myname target_directory\n";
  exit(1);
}

$directories = array();
$files = array();
readpath($target_dir, 1, 100, $directories, $files);
$files = array_filter($files, 'filter_code_files');
foreach ($files as $file) {
  $contents = file_get_contents($file);

  $contents = preg_replace("/define\('DRUPAL_ROOT', getcwd\(\)\);/", "define('DRUPAL_ROOT', getcwd());\ndefine('DRUPAL_CORE', DRUPAL_ROOT . '/core');\ndefine('DRUPAL_SITES', DRUPAL_ROOT . '/sites');", $contents);

  $contents = preg_replace("/require_once DRUPAL_ROOT /", "require_once DRUPAL_CORE ", $contents);
  $contents = preg_replace("/require DRUPAL_ROOT /", "require DRUPAL_CORE ", $contents);
  $contents = preg_replace("/include_once DRUPAL_ROOT /", "include_once DRUPAL_CORE ", $contents);
  $contents = preg_replace("/include DRUPAL_ROOT /", "include DRUPAL_CORE ", $contents);
  $contents = preg_replace("/install.php/", "core/install.php", $contents);
  $contents = preg_replace('/file_exists($filename)/', 'file_exists(DRUPAL_CORE . $filename)', $contents);

  // These . need to be replaced with proper $ characters.
  $contents = preg_replace('/"profiles\/.profile\/.directory"/', '"core/profiles/$profile/$directory"', $contents);
  $contents = str_replace('$searchdir = array($directory);', '$searchdir = array("core/" . $directory);', $contents);

  // The preg_replace() inserted here should be optimized.
  $contents = preg_replace("/.base_path = .dir/", "\$dir = preg_replace('/core$/', '', \$dir);\n      \$base_path = \$dir", $contents);

  $contents = preg_replace("/file_scan_directory\('\.\/profiles'/", "file_scan_directory(DRUPAL_CORE . '/profiles'", $contents);

  $contents = preg_replace("/drupal_parse_info_file\(\"profiles/", 'drupal_parse_info_file(DRUPAL_CORE . "/profiles', $contents);
  $contents = preg_replace("/drupal_parse_info_file\(dirname/", "drupal_parse_info_file(DRUPAL_CORE . '/' . dirname", $contents);

  $contents = preg_replace("/.profile_file = DRUPAL_ROOT/", '$profile_file = DRUPAL_CORE', $contents);

  $contents = preg_replace("/file_scan_directory\(DRUPAL_ROOT/" , "file_scan_directory(DRUPAL_CORE", $contents);

  $contents = preg_replace("/DRUPAL_ROOT . '\/' . drupal_get_path/", "drupal_get_path", $contents);

  file_put_contents($file, $contents);
}

system("mkdir -p $target_dir/core/doc");
system("cd $target_dir && git mv CHANGELOG.txt COPYRIGHT.txt INSTALL*.txt LICENSE.txt MAINTAINERS.txt UPGRADE.txt core/doc");
system("cd $target_dir && git mv authorize.php cron.php includes install.php misc modules profiles scripts themes update.php web.config xmlrpc.php core");
// Add a chdir('..'); to all php files in /core so they have the same directory as
// index.php.
$directories = array();
$files = array();
readpath("$target_dir/core", 1, 1, $directories, $files);
$files = array_filter($files, 'filter_php_files');
foreach ($files as $file) {
  $contents = file_get_contents($file);
  $contents = preg_replace("/define\('DRUPAL_ROOT', getcwd\(\)\);/", "chdir('..');\ndefine('DRUPAL_ROOT', getcwd());", $contents);
  file_put_contents($file, $contents);
}

system("./change.sh $target_dir/core/includes");

function readpath($dir, $level, $last, &$dirs, &$files) {
  $dp = opendir($dir);
  while (FALSE != ($file=readdir($dp)) && $level <= $last) {
    if ($file != "." && $file != "..") {
      if (is_dir("$dir/$file")) {
        readpath("$dir/$file", $level+1, $last, $dirs, $files); // uses recursion
        $dirs[] = "$dir/$file";  // reads the dir into an array
      }
      else {
        $files[] = "$dir/$file"; // reads the file into an array
      }
    }
  }
}

function filter_code_files($var) {
  if (preg_match('/theme.inc$/', $var)) {
    return FALSE;
  }
  if (preg_match('/.php$/', $var)) {
    return TRUE;
  }
  if (preg_match('/.inc$/', $var)) {
    return TRUE;
  }
  if (preg_match('/.module$/', $var)) {
    return TRUE;
  }
  if (preg_match('/.install$/', $var)) {
    return TRUE;
  }
  if (preg_match('/.tpl.php$/', $var)) {
    return TRUE;
  }
  if (preg_match('/.test$/', $var)) {
    return TRUE;
  }
  // This is for password-hash.sh and run-tests.sh.
  if (preg_match('/.sh$/', $var)) {
    return TRUE;
  }
  // .engine is excluded as it may include files from /core or /sites, so it must be DRUPAL_ROOT.
  return FALSE;
}

function filter_php_files($var) {
  if (preg_match('/.php$/', $var)) {
    return TRUE;
  }
  return FALSE;
}

